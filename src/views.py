#!/usr/bin/env python

from flask import Flask
from flask import render_template
from . import app


@app.route("/")
def home():
    return render_template("home.html")


# New functions
@app.route("/about/")
def about():
    return render_template("about.html")
