import flask
from flask_frozen import Freezer

app = flask.Flask(__name__)
base_url = 'https://weversonvn.gitlab.io/'
app.config['FREEZER_BASE_URL'] = base_url
app.config['FREEZER_DESTINATION'] = '../public'
freezer = Freezer(app)
