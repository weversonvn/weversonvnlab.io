#!/usr/bin/env python

from . import app
from . import freezer
from . import views

@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()
